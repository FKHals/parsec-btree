import Data.Tree hiding (Tree )
import Text.ParserCombinators.Parsec

data BTree a = Leaf a | Branch a (BTree a) (BTree a) deriving Show

{--
In dieser Implementierung wird die Wurzel als Knoten mitgezaehlt.
Ausserdem gehen wir davon aus, dass auch Blaetter Knoten sind, zu denen der Weg mitzaehlt.
Laesst erstmal 3 Testlaeufe durchlaufen und danach kann man einen eigenen Baum in der
Konsole eingeben
--}

-- Testeingaben (Als Either-Typ um die gleichen Funktionen nutzen zu können wir bei der
-- Eingabe durch den parser)
t1 = Right $ Branch "5" (Branch "6" (Branch "7" (Leaf "B") (Leaf "B")) (Leaf "B")) ( Branch "3" (Leaf "B") (Leaf "B"))
t2 = Right $ Branch "10" (Branch "12" (Branch "16" (Leaf "B") (Leaf "B")) (Leaf "B")) (Branch "5" (Branch "7" (Leaf "B") (Leaf "B")) (Branch "1" (Leaf "B") (Leaf "B")))
t3 = Right $ Branch "4" (Branch "6" (Leaf "B") (Leaf "B")) ( Branch "2" (Leaf "B") (Leaf "B"))

-- zaehlt Knoten
countNodes :: BTree a -> Int
countNodes (Leaf _)               = 1
countNodes (Branch a left right ) = 1 + countNodes left + countNodes right

-- akkumuliert alle Wege zu allen Knoten
countPaths :: BTree a -> Int
countPaths tree = countPaths' tree 0
    where countPaths' (Leaf _)               depth = depth
          countPaths' (Branch a left right ) depth = depth + (countPaths' left (depth+1)) + (countPaths' right (depth+1))

-- zaehlt durchschnittliche Wege pro Knoten
midNodePath :: BTree a -> Double
midNodePath t = (fromIntegral (countPaths t)) / (fromIntegral (countNodes t))


-----------------------------------------------------------------------------------------
-- fuer die "grafische" Darstellung:

toDataTree (Leaf a) = Node a []
toDataTree (Branch b cs ds) = Node b [toDataTree cs, toDataTree ds]


-----------------------------------------------------------------------------------------
-- BTree parser (mit Hilfe von parsec):

parseBrackets :: Parser (BTree String)
parseBrackets = do
    char '('
    a <- parseTree
    char ')'
    return (a)

parseNode :: Parser (BTree String)
parseNode = do
    string "Branch "
    a <- many1 alphaNum
    char ' '
    left <- parseTree
    char ' '
    right <- parseTree
    return (Branch a left right)

parseLeaf :: Parser (BTree String)
parseLeaf = do
    string "Leaf "
    a <- many1 alphaNum
    return (Leaf a)

parseTree = (parseLeaf <|> parseNode <|> parseBrackets)

parser :: String -> Either ParseError (BTree String)
parser input = parse parseTree "Fehlerhafte Eingabe!" input


-----------------------------------------------------------------------------------------
-- gibt resultate aus
result :: Either ParseError (BTree String) -> IO ()
result (Left err) = putStrLn $ "Parse Error: " ++ show err
result (Right t) = do
    putStrLn $ "Darstellung:\n" ++ (drawTree $ toDataTree t)
    putStrLn $ "Summe aller Pfade: " ++ (show $ countPaths t)
    putStrLn $ "Knotenanzahl: " ++ (show $ countNodes t)
    putStrLn $ "mittlerer Weg zwischen inneren Knoten: " ++ (show $ midNodePath t)


-----------------------------------------------------------------------------------------

main = do
    putStrLn "Beispiele:"
    result t1
    result t2
    result t3
    putStrLn "Nun kannst du auch einen eigenen Baum folgender Form angeben:"
    putStrLn "BTree a = Leaf a | Branch a (BTree a) (BTree a)"
    custom <- getLine
    --parseTest parseTree custom
    result $ parser custom
