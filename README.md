# Parsec BTree

Example of a B-tree implementation, including a function to get the mean internal (between inner nodes) path length. Also includes a parsec-parser for that B-Tree.
